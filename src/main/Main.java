package main;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        String path = "C://Users//web//Desktop//newDir";
        String[] strings = {"STRING 1", "STRING 2", "STRING 3", "STRING 4", "STRING 5"};


        File d = main.createDir(path);
        String str = d.getAbsolutePath();
        File f = main.createFile(str, "first");
        f = main.renameFile(f);
        main.writeFile(strings, f);
        System.out.println(main.readFile(f));
        main.writeFile("STRING 6", f);
        System.out.println(main.readFile(f));
//        main.deleteFile(f);
//        main.deleteDir(d);
    }


    public File createFile(String path, String filename){
        try {
            File file = new File(path,filename+".txt");
            if(!file.exists()){
                file.createNewFile();
                System.out.println("Файл успешно создан");
            }
            return file;
        } catch (Exception ex){
            System.out.println("Файл не создан");
            return  null;
        }
    }


    public File createDir(String path){
        try {
            File dir = new File(path);
            if(!dir.exists()){
                dir.mkdir();
            }
            System.out.println("Каталог успешно создан");
            return dir;
        } catch (Exception ex){
            System.out.println("Каталог не создан");
            return  null;
        }
    }

    public File renameFile(File file){
//        File newFile = new File("C://Users//web//Desktop//newDir//renamed.txt");

        File newFile = new File(file.getAbsolutePath() + ".txt");
        if (file.renameTo(newFile)){
            System.out.println("Файл успешно переименован");
        } else {
            System.out.println("Файл не переименован");
        }
        return file;
    }

    public void deleteDir(File file){
        if (file.delete()){
            System.out.println("Каталог успешно удален");
        } else {
            System.out.println("Каталог не удален");
        }
    }

    public void deleteFile(File file){
        if (file.delete()){
            System.out.println("Файл успешно удален");
        } else {
            System.out.println("Файл не удален");
        }
    }

    public String readFile(File file){
        try {
            FileReader fileReader =new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = new String();
            while (bufferedReader.ready()){
                str += bufferedReader.readLine() + " ";
            }
            bufferedReader.close();
            return str;
        } catch (Exception ex){
            return "Что-то не так с чтением";
        }
    }

    public void writeFile(String[] text, File file){
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (String s: text) {
                bufferedWriter.write(s);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception ex){
            System.out.println("Что-то не так с записью массива");
        }
    }

    public void writeFile(String text, File file){
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(text);
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception ex){
            System.out.println("Что-то не так с записью строки");
        }
    }
}